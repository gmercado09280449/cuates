/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stateless;

import entidad.Libro;
import java.math.BigDecimal;
import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author gabo
 */
@Stateless
public class LibroBean implements LibroBeanRemote {
    @PersistenceContext(name="LibreriaJEETop-ejbPU")

    EntityManager em ;
    Libro libro;
    Collection <Libro> listalibros;
    
    
    @Override
    public void addLibros(String titulo, String autor, BigDecimal precio) {
        if(libro==null)
        {
            libro= new Libro( titulo, autor, precio);
            em.persist(libro);
            libro=null;
        }
    }

    @Override
    public Collection<Libro> getAllLibros() {
        listalibros = em.createNamedQuery("Libro.findAll").getResultList();
        return listalibros;
        
    }
    
         public Libro buscaLibro(int id)
    {
        libro=em.find(Libro.class, id);
        /*
        System.out.println("Regresando Libro");
        if(libro!=null)
            System.out.println("Libro hallado "+libro.getTitulo());
        else
            System.out.println("Libro no hallado");
        */
        return libro;
    }
         
            public void actualizaLibro(Libro libro, String Titulo, String autor, BigDecimal precio)
    {
        if(libro!=null)
        {
            System.out.println("Actualizacion de libro  libro");
            libro.setTitulo(Titulo);
            libro.setAutor(autor);
            libro.setPrecio(precio);
            em.merge(libro);
            }
    }
            
           public void eliminaLibro(int id)
    {
         libro=em.find(Libro.class, id);
       
        {
            System.out.println("Eliminando el Registro");
          
            em.remove(libro);
            libro=null;
            
        }
    }     
            
        
         

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    
    
    
}
