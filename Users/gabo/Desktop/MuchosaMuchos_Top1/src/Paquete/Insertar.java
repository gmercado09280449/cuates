/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Paquete;

import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author gabo
 */
public class Insertar {
    
     public Orden InsertarOrden()
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("MuchosaMuchosPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        
        Orden orden1 = new Orden();
        java.util.Date utilDate=new java.util.Date();
        java.sql.Date sqlDate=new java.sql.Date(utilDate.getTime());
        orden1.setFechaOrden(sqlDate);
        try
        {
            em.persist(orden1);
        }
        catch (Exception e)
        {
        }
        try
        {
            em.getTransaction().commit();
        }
        catch (Exception e)
        {
            System.out.println("Ocurrio el error" + e);
        }
        em.close();
        emf.close();
        return orden1;
    }
    public Producto InsertarProducto()
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("MuchosaMuchosPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        
        Producto producto1 = new Producto();
        producto1.setDescripcion("Este es la descripcion del producto");
        Float precio =50.0f;
      
                   producto1.setPrecio(precio);
        try
        {
            em.persist(producto1);
        }
        catch (Exception e)
        {
        }
        try
        {
            em.getTransaction().commit();
        }
        catch (Exception e)
        {
            System.out.println("Ocurrio el error" + e);
        }
        em.close();
        emf.close();
        return producto1;
    }
    public Lineaorden InsertarLinea(Orden orden,Producto producto)
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("MuchosaMuchosPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        
        
        Lineaorden lineaorden1 = new Lineaorden(orden.getIdOrden(),producto.getIdProducto());
        lineaorden1.setCantidad(12);
        
        try
        {
            em.persist(lineaorden1);
        }
        catch (Exception e)
        {
        }
        try
        {
            em.getTransaction().commit();
        }
        catch (Exception e)
        {
 	           System.out.println("Ocurrio el error" + e);
        }
        em.close();
        emf.close();
        return lineaorden1;
    }
    public void Desplegar(Orden orden,Producto producto,Lineaorden lineaorden)
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("MuchosaMuchosPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
            Collection<Orden> listaord;
            listaord = em.createNamedQuery("Orden.findAll").getResultList();
        System.out.println("**Orden**");
        for (Orden elemento : listaord)
        {
      System.out.println("ID orden: "+elemento.getIdOrden()+"\nFecha orden: "+elemento.getFechaOrden());
            System.out.println("-                               -");
        
        }        
        System.out.println("**Producto**");
        Collection<Producto> listaprod;
        listaprod = em.createNamedQuery("Producto.findAll").getResultList();

        for (Producto el : listaprod)
        {
            System.out.println("ID:"+el.getIdProducto()+"\nDescripcion: "+el.getDescripcion()+"\nPrecio: "+el.getPrecio());
            System.out.println("-                                                 -");
        }
        System.out.println("**Linea Orden**");
        Collection<Lineaorden> listalinea;
        listalinea = em.createNamedQuery("Lineaorden.findAll").getResultList();

        for (Lineaorden el : listalinea)
        {
            System.out.println("ID lineaorden: "+el.getOrden().getIdOrden()+"\nID  de producto: "+el.getProducto().getIdProducto()+"\nCantidad: "+el.getCantidad());
            System.out.println("-                        -");
        }
       
    }
    
    
}
